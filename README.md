
# unredat

Programa para leer los datos almacenados en los multímetros Uni-T.

## Descripción

Muchos de los multímetros de Uni-T puden almacenar mediciones de forma independiente,
sin tener que estar conectado a un ordenador(Datalogger).

El problema con mi multímetro Uni-T UT71D, es que en la versión europea hay
un bug que impide leer los datos almacenados con el programa oficial. Cambiando
el cable para usar el protocolo serie se pueden leer los datos en crudo (RAW).
Y con una hoja de cálculo se pueden decodificar. Esto se explica en detalle
en el [blog.](https://www.circuiteando.net/2018/06/hack-cable-para-uni-t-ut71d.html)

Con este programa se pueden leer los datos en Linux sin tener que configurar el puerto
serie, así como decodificar los datos en un archivo de texto para verlos de forma
fácil y rápida, sin necesidad de la hoja de cáculo (A menos que se quieran hacer gráficas).

### Aviso

El programa se encuentra en estado beta. Se ha probado únicamente con el modelo UT71D y
mayormente en la función de VDC, puede que haya algún problema en las otras funciones o rangos.

## Uso

Para ver las opciones disponibles:

    $ unredat --help

![ayuda](https://3.bp.blogspot.com/-NjYttuyyby4/W1H0s9RZlqI/AAAAAAAABeM/psG_aM5wTK4-EiVxczIN95mQGBgBuLRFACKgBGAs/s640/ayuda.png)

Para utilizarlo es sufiente con ejecutarlo sin argumentos:

    $ unredat

![comandos](https://4.bp.blogspot.com/-9w4UhF-tiR0/W1Hx0jXMDDI/AAAAAAAABdk/rYgGhvf9Vi0ICIJakwlDM3wOwZ3FObiuwCKgBGAs/s640/comandos.png)

El programa busca automáticamente el multímetro  (El chip CH341 específicamente) y
se pone a la espera. Una vez se mandan los datos desde el multímetro, se guardan en
el archivo especificado. En caso de no proporcionar uno se guardan en Uni-T_Datos.txt.

Para decodificar los datos guardados y crear una tabla con los mismos:

    $ unredat --tabla FICHERO

Esto crea un archivo con el formato ARCHIVO_tabla, que contiene los datos.

Muestra del archivo con los datos en crudo (RAW):

![datos](https://2.bp.blogspot.com/-t-ewBoOj0b0/W1HySQQUQeI/AAAAAAAABds/Xm8wX_7RPb8TaxGAamfOTc1jzB2zKTD2gCKgBGAs/s640/datos.png)

Muestra del archivo con los datos decodificados:

![datos_tabla](https://3.bp.blogspot.com/-62zi4nIbkG0/W1HzELJacRI/AAAAAAAABd0/nGOFrNgFF10KFAki--e5W52-YH8EZvQ9wCKgBGAs/s640/datos_tabla.png)

Se puede indicar la fecha y hora en la que se tomaron las medidas y el intervalo
entre las mismas. De esta forma se añade la fecha y hora correspondiente a cada medida.

    $ unredat --tabla FICHERO --tinicio "dd/mm/aa hh:mm:ss" --invervalo x

![comando_tabla_fecha](https://3.bp.blogspot.com/-SkXj2gHeLIg/W1H0CbusnhI/AAAAAAAABd8/iKTzNYMWH3g-d9-W93W24JMuPxbM6Z9kQCKgBGAs/s640/comando_tabla_fecha.png)

Y se obtiene lo siguiente:

![datos_taba_fecha](https://4.bp.blogspot.com/-ZztXT4WWtkY/W1H0ShPKSkI/AAAAAAAABeE/l6amLHNLhCw9cCf5V6R89oEwIp3GgtHiQCKgBGAs/s640/datos_tabla_fecha.png)

Para leer los datos y crear la tabla en un solo comando:

    $ unredat --captura --tabla [FICHERO]

Si no se reconoce el multímetro. (Chip diferente del CH341) Se puede indicar el puerto
serie a utilizar. Por ejemplo:

    $ unredat --puerto /dev/ttyUSB5

## Instalación

### Como ejecutable

Para instalarlo en una distribución Linux basta con copiar el archivo unredat de la carpeta
dist. Puede que no funcione en distribuciones con una versión antigua de la librería glibc.
Ha sido probado en Linux Debian 9 (64 bits).

### Como script

Copiando solamente el archivo principal unredat.py ubicado dentro de la carpeta src/unredat.

Se requiere de los módulos click, pyserial y colorama. Para más información mirar el
archivo requirements.txt.

## Desarrollo

Para portarlo a otras plataformas distintas de Linux o ayudar a su desarrollo se puede crear
un entorno aislado mediante conda. En él se encuentra la versión de python y todas los módulos
utilizidados durante el desarrollo, por lo que no habrá problemas de dependencias.

    $ conda env create -f conda_enviroment.yml

Para crear un ejecutable único, con el interprete de python y todas las dependencias incluidas, se puede utilizar PyInstaler.

En mi caso utilicé el siguiente comando:

    $ python -OO -m PyInstaller --onefile ./src/unredat/unredat.py

Para crear la documentación:

    $ python setup.py docs

Para correr los tests:

    $ python setup.py test


## Licencia

Este proyecto está bajo licencia MIT.

## Nota

Puede obtener información adicional en el [blog.](https://www.circuiteando.net/2018/07/unredat.html)

Este proyecto ha sido inicializado mediante PyScaffold 3.0.3. Para más detalles
y uso sobre PyScaffold ver http://pyscaffold.org/.



