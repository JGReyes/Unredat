# -*- coding: utf-8 -*-
# File: unredat.py
# Project: unredat
# Created Date: Friday, July 13th 2018, 10:43:51 am
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# MIT License
#
# Copyright (c) 2018 JGReyes
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


import click
import serial
import serial.tools.list_ports as ports
from collections import namedtuple
import os.path
from datetime import datetime, timedelta


class Multimetro:

    def recolectar_datos(self, puerto='', fichero='Uni-T_Datos.txt', tiempo_espera=2):
        """ Lee los datos del multímetro y los guarda en un archivo.
        Si no se le indica el puerto, se busca automáticamente el chip USB CH341
        y se utiliza el puerto de este dispositivo.
        Al terminar informa de los registros guardados en el fichero y los que
        presentan un error de formato.

        :param puerto: puerto de comunicación serie, por defecto ''.
        :param puerto: str, opcional
        :param fichero: fichero donde guardar los datos, por defecto 'Uni-T_Datos.txt'.
        :param fichero: str, opcional
        :param tiempo_espera: tiempo en segundos antes de dejar de bloquear al leer
         desde el puerto serie, por defecto 2.
        :param tiempo_espera: int, opcional
        :return: True en caso de leer y guardar registros, False en caso de error.
        :rtype: bool
        """

        if not puerto:
            click.echo('Buscando multimetro...')
            # Busca el pid y vid del chip usb CH341
            multimetro = [puerto.device for puerto in ports.comports()
                          if puerto.vid == 0x1a86 and puerto.pid == 0x7523]
            if multimetro:
                puerto = multimetro[0]
                click.echo('Multímetro encontrado en {}'.format(puerto))
            else:
                click.echo(click.style('Multímetro NO encontrado.', fg='red'))
                return False
        try:
            ser = serial.Serial(port=puerto, timeout=1,
                                baudrate=2400,
                                parity=serial.PARITY_ODD,
                                bytesize=serial.SEVENBITS,
                                stopbits=serial.STOPBITS_ONE,
                                )
            click.echo('Conectado a {}'.format(puerto))
            registros = []
            leyendo = False
            nbytes_vacios = 0
            while True:
                dato = ser.read(size=11)
                if not dato or dato[0] == b'':
                    if leyendo:
                        nbytes_vacios += 1
                        if nbytes_vacios > 2:  # Se da por terminada la transmisión
                            break
                else:
                    if not leyendo:
                        leyendo = True
                        click.echo('Recibiendo datos...')
                    registros.append(dato)
            res = self._guardar(registros, fichero)
            if len(res.erroneos) > 0:
                for error in res.erroneos:
                    click.echo(click.style('Dato No. {} no válido'.format(error),
                                           fg='red'))
            click.echo(click.style('Escritura correcta de {} valores en {}'.format(
                res.escritos, fichero), fg='green'))
            return True
        except serial.SerialException:
            click.echo(click.style('Error: No se puede conectar con {}'.format(
                puerto), fg='red'))
            return False
        except Exception as e:
            click.echo(click.style('Error inexperado: {}'.format(str(e)),
                                   fg='red'))
            return False
        finally:
            if 'ser' in locals():
                if ser.is_open:
                    ser.close()

    def _convertir_a_texto(self, bytes):
        """ Convierte los bytes recibidos en cadenas de texto.

         :param bytes: bytes recibidos del multímetro.
         :type bytes: bytes
         :return: los registros de memoria del multímetro.
         :rtype: str
        """

        # Los dos últimos bytes no contienen información, por lo que se desechan.
        text_num = str(bytes[0:-2])
        numero = [caracter for caracter in text_num if caracter.isdigit()]
        return ''.join(numero)

    def _guardar(self, registros, fichero):
        """ Se encarga de comprobar que los registros recibidos son correctos comprobando
        el último byte de cada uno y en caso afirmativo lo guarda en el fichero
        indicado, de en caso contrario guarda el número de registro del multímetro
        que no es correcto para poder mostrarlo posteriormente.

        :param registros: registros a guardar.
        :type registros: list de bytes
        :param fichero: archivo donde guardar los registros.
        :type fichero: str
        :return: nametuple con el número de registros guardados correctamente y una
         lista con los números de registro que tienen errores y no se han
         guardado.
        :rtype: Resultado
        """

        Resultado = namedtuple('Resultado', 'escritos erroneos')
        with open(fichero, 'w') as f:
            nregistro = 1
            registros_erroneos = []
            for registro in registros:
                if registro[-1] != 138:  # Comprueba que es un registro válido
                    registros_erroneos.append(nregistro)
                    nregistro += 1
                    continue
                linea = self._convertir_a_texto(registro)
                f.write(''.join([linea, '\n']))
                nregistro += 1
            resultado = Resultado((nregistro-1)-len(registros_erroneos),
                                  tuple(registros_erroneos))
            return (resultado)

    def crear_tabla(self, fichero, tiempo=None, intervalo=1):
        """ Decodifica los datos en crudo (RAW) del fichero especificado, crea
        una tabla legible con los valores y modos del multímetro y la guarda
        en un fichero de texto.

        :param fichero: fichero con los datos en crudo (RAW).
        :type fichero: str
        :param tiempo: fecha y hora de inicio de los registros.
        :type tiempo: datetime
        :param intervalo: intervalo en segundos entre cada registro.
        :type intervalo: int
        :return: True si finaliza correctamete, False en caso de error.
        :rtype: bool
        """

        lineas = []
        salida = []
        sf_tiempo = ''
        if os.path.exists(fichero):
            with open(fichero, 'r') as f:
                lineas = f.readlines()
            if lineas:
                with click.progressbar(lineas, label='Creando tabla') as lineas_bar:
                    for linea in lineas_bar:
                        longitud = len(linea[:-1])
                        lcd = linea[0:5]
                        if longitud == 9:
                            ran_y_fun = linea[5:7]
                            st_acdc = linea[7]
                            st_atuo = linea[8]
                        elif longitud == 10:
                            ran_y_fun = linea[5:8]
                            st_acdc = linea[8]
                            st_atuo = linea[9]
                        else:
                            continue

                        if isinstance(tiempo, datetime):
                            tiempo += timedelta(seconds=intervalo)
                            sf_tiempo = tiempo.strftime('%H:%M:%S %d/%m/%y')

                        salida.append(self._formatear_registro(
                            lcd, ran_y_fun, st_acdc, st_atuo, sf_tiempo))
                if salida:
                    nombre_completo = fichero.split(os.sep)[-1]
                    nombre, ext = nombre_completo.split('.')
                    fichero_salida = ''.join([
                        os.path.dirname(fichero), os.sep,
                        nombre, '_tabla.', ext])

                    if os.path.dirname(fichero) == '':
                        fichero_salida = fichero_salida[1:]

                    try:
                        with open(fichero_salida, 'w') as f:
                            cabecera = '{5:>2} {0:^8} {1:^10} {2:^10} {3:^7} {4:^8} {6:^19}\n'.format(
                                'Valor', 'Modo', 'Rango', 'AC+DC', 'Auto-rango', 'S', 'Hora y fecha')
                            f.write(cabecera)
                            f.write('-' * len(cabecera) + '\n')
                            f.writelines(salida)
                        return True
                    except PermissionError as e:
                        click.echo(click.style('Error al guardar la tabla: {}'.format(
                            str(e)), fg='red'))
                else:
                    return False
            else:
                return False
        else:
            return False

    def _formatear_registro(self, lcd, ryf, acdc, atuo, tiempo=''):
        """ Decodifica un registro del multímetro y lo devuelve como linea de
        texto formateada en columnas.

        :param lcd: número correspondiente a los digitos de la pantalla lcd.
        :type lcd: str
        :param ryf: número que indica el rango y función seleccionada.
        :type ryf: str
        :param acdc: número que indica el estado del botón AC+DC.
        :type acdc: str
        :param atuo: número que indica el estado del signo y el auto-rango.
        :type atuo: str
        :param tiempo: fecha y hora cuando se tomó el registro.
        :type tiempo: str
        :return: linea de texto formateada en columnas.
        :rtype: str
        """

        rango_y_funcion = {'00': ('AC_mV', '400 mV', 100),
                           '11': ('DCV', '4 V', 10000),
                           '21': ('DCV', '40 V', 1000),
                           '31': ('DCV', '400 V', 100),
                           '41': ('DCV', '1000 V', 10),
                           '12': ('ACV', '4 V', 10000),
                           '22': ('ACV', '40 V', 1000),
                           '32': ('ACV', '400 V', 100),
                           '42': ('ACV', '750 V', 10),
                           '03': ('DC_mV', '400 mV', 100),
                           '14': ('Ohm', '400 Ohm', 100),
                           '24': ('Ohm', '4 K', 10000),
                           '34': ('Ohm', '40 K', 1000),
                           '44': ('Ohm', '400 K', 100),
                           '54': ('Ohm', '4 M', 10000),
                           '64': ('Ohm', '40 M', 1000),
                           '15': ('Cap', '40 nF', 1),
                           '25': ('Cap', '400 nF', 100),
                           '35': ('Cap', '4 uF', 1),
                           '45': ('Cap', '40 uF', 1),
                           '55': ('Cap', '400 uF', 100),
                           '65': ('Cap', '4 mF', 10),
                           '75': ('Cap', '40 mF', 1),
                           '06': ('Temp C', '1000 C', 10),
                           '07': ('uA', '400 uA', 100),
                           '17': ('uA', '4000 uA', 10),
                           '08': ('mA', '40 mA', 1000),
                           '18': ('mA', '400 mA', 100),
                           '19': ('10 A', '10 A', 1000),
                           '010': ('Fm', '', 1),
                           '011': ('Diode', '', 10),
                           '012': ('Hz', '40 Hz', 1000),
                           '112': ('Hz', '400 Hz', 100),
                           '212': ('Hz', '4 Khz', 10),
                           '312': ('Hz', '40 Khz', 1),
                           '412': ('Hz', '400 Khz', 0.1),
                           '512': ('Hz', '4 Mhz', 0.01),
                           '612': ('Hz', '40 Mhz', 0.001),
                           '712': ('Hz', '400 Mhz', 0.0001),
                           '015': ('%(4-20mA)', '', 1),
                           '016': ('Temp F', '1000 F', 10)}

        estado_ac_dc = {'0': 'OFF',
                        '1': 'AC',
                        '2': 'DC',
                        '3': 'AC+DC'}

        numero = int(lcd)
        modo, rango, divisor = rango_y_funcion[ryf]
        valor = numero / divisor
        ac_dc = estado_ac_dc[acdc]
        atuo = int(atuo)
        signo = ''
        auto_rango = ''

        s = atuo & 4       # b'100'
        if s == 0:
            signo = '+'
        elif s == 4:
            signo = '-'

        a = atuo & 3       # b'011'
        if a == 0:
            auto_rango = 'OFF'
        elif a == 1:
            auto_rango = 'AUTO'
        elif a == 2:
            auto_rango = 'Manual'

        if not tiempo or tiempo == 'None':
            registro = '{4:>2} {0:^8.4f} {1:^10} {2:^10} {3:^7} {5:^8}\n'.format(
                valor, modo, rango, ac_dc, signo, auto_rango)
        else:
            registro = '{4:>2} {0:^8.4f} {1:^10} {2:^10} {3:^7} {5:^8} {6:^23}\n'.format(
                valor, modo, rango, ac_dc, signo, auto_rango, tiempo)
        return registro


def _validar_fecha_hora(ctx, parametro, valor):
    """ Valida la fecha y hora del parámetro --tinicio.

    :param ctx: contexto del parámetro.
    :type ctx: click.context
    :param parametro: parametro a validar.
    :type parametro: click.parameter
    :param valor: valor del parametro a validar.
    :type valor: str
    :raises click.BadParameter: si la fecha y hora no se ajustan al formato.
    :return: fecha y hora.
    :rtype: datetime
    """

    if valor != None:
        try:
            tinicio = datetime.strptime(valor, '%d/%m/%y %H:%M:%S')
            return tinicio

        except ValueError:
            raise click.BadParameter('La fecha y/o hora no son correctos. El'
                                     + ' formato debe ser "dd/mm/aa hh:mm:ss".')


@click.command()
@click.option('-i', '--intervalo', default=1, help='Intervalo de tiempo entre '
              + 'registros. En segundos.(Por defecto 1)', type=int)
@click.option('-ti', '--tinicio', help='Fecha y hora en formato "dd/mm/aa hh:mm:ss"'
              + ' en la que se empezó a tomar datos, para añadir el timestamp'
              + ' a cada registro.', callback=_validar_fecha_hora)
@click.option('-c', '--captura', is_flag=True, help='Inicia la captura de datos.')
@click.option('-t', '--tabla', is_flag=True, help='Guarda una tabla con los'
              + 'datos decodificados en un archivo terminado en _tabla')
@click.argument('fichero', default='Uni-T_Datos.txt')
@click.option('-p', '--puerto', default='', help='Puerto donde está conectado'
              + 'el multímetro. De no indicarse se busca de forma automática')
def main(intervalo, tinicio, captura, tabla, fichero, puerto):
    """ Programa para leer los datos de los multímetros Uni-T por el
    puerto serie y guardarlos en un archivo (Uni-T_Datos.txt por defecto).
    También puede crear una tabla con los datos decodificados procedentes del
    archivo indicado, el archivo resultante tiene el nombre del indicado más
    _tabla.
    """
    multi = Multimetro()

    if captura:
        multi.recolectar_datos(puerto=puerto, fichero=fichero)

    if tabla:
        if os.path.exists(fichero):
            if isinstance(tinicio, datetime):
                res = multi.crear_tabla(
                    fichero, tiempo=tinicio, intervalo=intervalo)
            else:
                res = multi.crear_tabla(fichero)
            if res:
                click.echo(click.style(
                    'Tabla creada correctamente.', fg='green'))
            else:
                click.echo(click.style(
                    'Error al guardar la tabla. Pude que los datos no tengan'
                    + ' el formato correcto.', fg='red'))
        else:
            click.echo(click.style(
                'El fichero {} no existe o no es accesible.'.format(fichero),
                fg='red'))

    if not captura and not tabla:
        multi.recolectar_datos(puerto=puerto, fichero=fichero)


if __name__ == '__main__':
    main()
