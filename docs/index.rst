=======
unredat
=======

Programa para leer los datos almacenados en los multímetros Uni-T.

Muchos de los multímetros de Uni-T puden almacenar mediciones de forma
independiente, sin tener que estar conectado a un ordenador. (Datalogger)

Con este programa se pueden leer los datos en Linux sin tener que configurar
el puerto serie, así como decodificar los datos en un archivo de texto para
verlos de forma fácil y rápida, sin necesidad de la hoja de cáculo
(A menos que se quieran hacer gráficas).

Aviso
=====

El programa se encuentra en estado beta. Se ha probado únicamente con el
modelo UT71D y mayormente en la función de VDC, puede que haya algún problema
en las otras funciones o rangos.


Uso
====

Para ver las opciones disponibles:

    $ unredat --help


Para utilizarlo es sufiente con ejecutarlo sin argumentos:

    $ unredat

El programa busca automáticamente el multímetro  (El chip CH341 específicamente)
y se pone a la espera. Una vez se mandan los datos desde el multímetro, se
guardan en el archivo especificado. En caso de no proporcionar uno se guardan
en Uni-T_Datos.txt.

Para decodificar los datos guardados y crear una tabla con los mismos:

    $ unredat --tabla FICHERO

Esto crea un archivo con el formato ARCHIVO_tabla, que contiene los datos.


Se puede indicar la fecha y hora en la que se tomaron las medidas y el intervalo
entre las mismas. De esta forma se añade la fecha y hora correspondiente a cada
medida.

    $ unredat --tabla FICHERO --tinicio "dd/mm/aa hh:mm:ss" --invervalo x

Para leer los datos y crear la tabla en un solo comando:

    $ unredat --captura --tabla [FICHERO]

Si no se reconoce el multímetro. (Chip diferente del CH341) Se puede indicar
el puerto serie a utilizar. Por ejemplo:

    $ unredat --puerto /dev/ttyUSB5

Contents
========

.. toctree::
   :maxdepth: 2

   License <license>
   Authors <authors>
   Changelog <changelog>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
