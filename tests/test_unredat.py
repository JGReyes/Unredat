# -*- coding: utf-8 -*-
# File: test_unredat.py
# Project: unredat
# Created Date: Friday, July 13th 2018, 11:04:27 am
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# MIT License
#
# Copyright (c) 2018 JGReyes
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


import pytest
import unredat
import os
from click.testing import CliRunner

# Extracto de 100 registros reales sacados del multímetro.
registros_multimetro = (b'\xb0\xb0\xb02\xb311\xb0\xb5\r\x8a',
                        b'\xb0\xb0\xb02\xb311\xb0\xb5\r\x8a',
                        b'\xb0\xb0\xb02411\xb0\xb5\r\x8a',
                        b'\xb0\xb0\xb02\xb311\xb0\xb5\r\x8a',
                        b'\xb0\xb0\xb02411\xb0\xb5\r\x8a',
                        b'\xb0\xb0\xb01\xb611\xb0\xb5\r\x8a',
                        b'\xb044\xb6\xb511\xb01\r\x8a',
                        b'1\xb978711\xb01\r\x8a',
                        b'2\xb5\xb64711\xb01\r\x8a',
                        b'\xb3\xb5\xb91811\xb01\r\x8a',
                        b'\xb0488\xb321\xb01\r\x8a',
                        b'\xb0\xb622\xb021\xb01\r\x8a',
                        b'\xb0\xb677\xb321\xb01\r\x8a',
                        b'\xb0821721\xb01\r\x8a',
                        b'\xb0\xb9\xb6\xb0\xb021\xb01\r\x8a',
                        b'111\xb3\xb321\xb01\r\x8a',
                        b'1278421\xb01\r\x8a',
                        b'1\xb3\xb9\xb6221\xb01\r\x8a',
                        b'1\xb51\xb3421\xb01\r\x8a',
                        b'1\xb6\xb98821\xb01\r\x8a',
                        b'18\xb6\xb5\xb021\xb01\r\x8a',
                        b'2\xb0\xb37721\xb01\r\x8a',
                        b'21\xb9\xb0\xb521\xb01\r\x8a',
                        b'228\xb6\xb621\xb01\r\x8a',
                        b'2287721\xb01\r\x8a',
                        b'2288\xb621\xb01\r\x8a',
                        b'2288721\xb01\r\x8a',
                        b'2288721\xb01\r\x8a',
                        b'2288\xb621\xb01\r\x8a',
                        b'2288\xb621\xb01\r\x8a',
                        b'2288\xb621\xb01\r\x8a',
                        b'2288721\xb01\r\x8a',
                        b'2288721\xb01\r\x8a',
                        b'22\xb9\xb3821\xb01\r\x8a',
                        b'2\xb3\xb64\xb621\xb01\r\x8a',
                        b'2\xb3\xb9\xb5\xb921\xb01\r\x8a',
                        b'2\xb3\xb9\xb6\xb921\xb01\r\x8a',
                        b'2\xb3\xb9\xb6721\xb01\r\x8a',
                        b'2\xb6\xb38\xb521\xb01\r\x8a',
                        b'27\xb08\xb621\xb01\r\x8a',
                        b'27\xb9\xb3\xb521\xb01\r\x8a',
                        b'2\xb9\xb5\xb5821\xb01\r\x8a',
                        b'\xb3\xb02\xb3\xb021\xb01\r\x8a',
                        b'\xb31\xb04821\xb01\r\x8a',
                        b'\xb31\xb9\xb6721\xb01\r\x8a',
                        b'\xb3227721\xb01\r\x8a',
                        b'\xb3227821\xb01\r\x8a',
                        b'\xb3227\xb921\xb01\r\x8a',
                        b'\xb3227821\xb01\r\x8a',
                        b'\xb31\xb64\xb921\xb01\r\x8a',
                        b'2\xb6\xb9\xb0\xb321\xb01\r\x8a',
                        b'2\xb37\xb3\xb521\xb01\r\x8a',
                        b'21\xb62\xb521\xb01\r\x8a',
                        b'21\xb38421\xb01\r\x8a',
                        b'2\xb0\xb3\xb9221\xb01\r\x8a',
                        b'1\xb97\xb5721\xb01\r\x8a',
                        b'1\xb94\xb9421\xb01\r\x8a',
                        b'1\xb927\xb621\xb01\r\x8a',
                        b'18\xb91\xb621\xb01\r\x8a',
                        b'18\xb61121\xb01\r\x8a',
                        b'17\xb9\xb0821\xb01\r\x8a',
                        b'1\xb687721\xb01\r\x8a',
                        b'1\xb5\xb9\xb6\xb621\xb01\r\x8a',
                        b'1\xb5\xb04221\xb01\r\x8a',
                        b'14\xb37\xb521\xb01\r\x8a',
                        b'1\xb37\xb0121\xb01\r\x8a',
                        b'1\xb31\xb5221\xb01\r\x8a',
                        b'1242121\xb01\r\x8a',
                        b'11\xb61\xb321\xb01\r\x8a',
                        b'111\xb6821\xb01\r\x8a',
                        b'1\xb0\xb6\xb5221\xb01\r\x8a',
                        b'\xb0\xb9\xb9\xb9721\xb01\r\x8a',
                        b'\xb0\xb92\xb0121\xb01\r\x8a',
                        b'\xb0812\xb921\xb01\r\x8a',
                        b'\xb0\xb6\xb98\xb321\xb01\r\x8a',
                        b'\xb0\xb5\xb52421\xb01\r\x8a',
                        b'\xb0444821\xb01\r\x8a',
                        b'\xb04\xb01\xb921\xb01\r\x8a',
                        b'\xb3\xb021\xb611\xb01\r\x8a',
                        b'2\xb02\xb9\xb611\xb01\r\x8a',
                        b'1\xb0\xb0\xb3111\xb01\r\x8a',
                        b'\xb018\xb5811\xb01\r\x8a',
                        b'\xb0\xb0\xb3\xb3\xb911\xb01\r\x8a',
                        b'\xb0\xb01\xb3\xb611\xb01\r\x8a',
                        b'\xb0\xb0\xb07411\xb01\r\x8a',
                        b'1\xb3\xb61711\xb01\r\x8a',
                        b'282\xb6111\xb01\r\x8a',
                        b'282\xb5411\xb01\r\x8a',
                        b'28\xb32\xb611\xb01\r\x8a',
                        b'2\xb912111\xb01\r\x8a',
                        b'2\xb9\xb0\xb9711\xb01\r\x8a',
                        b'2\xb9\xb07111\xb01\r\x8a',
                        b'2\xb9\xb0\xb6711\xb01\r\x8a',
                        b'2\xb9\xb0\xb5\xb911\xb01\r\x8a',
                        b'2\xb9\xb0\xb6\xb011\xb01\r\x8a',
                        b'2\xb9\xb0\xb6\xb311\xb01\r\x8a',
                        b'2\xb9\xb0\xb6211\xb01\r\x8a',
                        b'2\xb9\xb0\xb6211\xb01\r\x8a',
                        b'2\xb9\xb0\xb6111\xb01\r\x8a',
                        b'2\xb9\xb0\xb6211\xb01\r\x8a')

# Valores correctos de los registros anteriores.
valores_correctos = ('000231105', '000231105', '000241105', '000231105',
                     '000241105', '000161105', '044651101', '197871101',
                     '256471101', '359181101', '048832101', '062202101',
                     '067732101', '082172101', '096002101', '111332101',
                     '127842101', '139622101', '151342101', '169882101',
                     '186502101', '203772101', '219052101', '228662101',
                     '228772101', '228862101', '228872101', '228872101',
                     '228862101', '228862101', '228862101', '228872101',
                     '228872101', '229382101', '236462101', '239592101',
                     '239692101', '239672101', '263852101', '270862101',
                     '279352101', '295582101', '302302101', '310482101',
                     '319672101', '322772101', '322782101', '322792101',
                     '322782101', '316492101', '269032101', '237352101',
                     '216252101', '213842101', '203922101', '197572101',
                     '194942101', '192762101', '189162101', '186112101',
                     '179082101', '168772101', '159662101', '150422101',
                     '143752101', '137012101', '131522101', '124212101',
                     '116132101', '111682101', '106522101', '099972101',
                     '092012101', '081292101', '069832101', '055242101',
                     '044482101', '040192101', '302161101', '202961101',
                     '100311101', '018581101', '003391101', '001361101',
                     '000741101', '136171101', '282611101', '282541101',
                     '283261101', '291211101', '290971101', '290711101',
                     '290671101', '290591101', '290601101', '290631101',
                     '290621101', '290621101', '290611101', '290621101')


@pytest.fixture
def multi():
    """ Devuelve una instancia de Multimetro para los tests

    """

    return unredat.Multimetro()


@pytest.fixture
def mock_multi(mocker):
    mock_multi = mocker.patch('unredat.unredat.Multimetro', autospec=True)
    mock_inst_multi = mock_multi.return_value
    return mock_inst_multi


@pytest.fixture
def runner():
    return CliRunner()


@pytest.fixture
def fichero(tmpdir):
    fichero = tmpdir.mkdir('unredat').join('datos.txt')
    with open(str(fichero), 'w') as f:
        f.write('256471101\n')
    return str(fichero)


def test_convertir_a_texto(multi):
    """  Comprueba la conversión de bytes a texto y que su longitud sea 9 o 10.

    :param multi: instancia de Multimetro
    :type multi: Multimetro
    """

    valores = tuple([multi._convertir_a_texto(registro)
                     for registro in registros_multimetro])
    for n, valor in enumerate(valores):
        assert valor == valores_correctos[n]
        assert 9 <= len(valor) <= 10


def test_guardar_val_correctos(multi, tmpdir, mocker):
    """ Comprueba que se guardan correctamente los valores en un fichero

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param tmpdir: permite crear directorios y archivos temporales
    :type tmpdir: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """

    fichero = tmpdir.mkdir('unredat').join('datos.txt')
    mock_convertir = mocker.patch(
        'unredat.Multimetro._convertir_a_texto', autospec=True)
    mock_convertir.side_effect = valores_correctos
    res = multi._guardar(registros_multimetro, str(fichero))
    assert res.escritos == 100
    assert len(res.erroneos) == 0
    with open(str(fichero), 'r') as f:
        registros = f.readlines()
        for n, registro in enumerate(registros):
            assert registro[0:-1] == valores_correctos[n]


def test_guardar_val_incorrectos(multi, tmpdir, mocker):
    """ Comprueba que se devuelve una tupla de los valores incorrectos y
    se guardan los correctos.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param tmpdir: permite crear directorios y archivos temporales
    :type tmpdir: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """

    valores = (b'\xb0\xb0\xb01\xb611\xb0\xb5\r\x8a',
               b'\xb044\xb6\xb511\xb01\r',
               b'1\xb978711\xb01\r\x8a',
               b'2\xb5\xb64711\xb01\r\x8a',
               b'\xb3\xb5\xb91811\xb01')

    valores_escritos = ('000161105', '197871101', '256471101')

    fichero = tmpdir.mkdir('unredat').join('datos.txt')
    mock_convertir = mocker.patch(
        'unredat.Multimetro._convertir_a_texto', autospec=True)
    mock_convertir.side_effect = valores_escritos
    res = multi._guardar(valores, str(fichero))
    assert res.escritos == 3
    assert len(res.erroneos) == 2
    assert res.erroneos == (2, 5)
    with open(str(fichero), 'r') as f:
        registros = f.readlines()
        for n, registro in enumerate(registros):
            assert registro[0:-1] == valores_escritos[n]


def test_recolectar_datos(multi, mocker):
    """ Comprueba que se detecta automáticamente el multimetro y se muestra
    el mensaje correspondiente al leer y guardar los registros de forma
    satisfactoria. Devuelve True.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """

    mock_comports = mocker.patch('serial.tools.list_ports.comports')
    mock_device = mocker.Mock(device='/dev/ttyUSB0', vid=0x1a86, pid=0x7523)
    mock_comports.return_value = [mock_device]
    mock_echo = mocker.patch('click.echo', autospec=True)
    mock_ser = mocker.patch('serial.Serial')
    registros = registros_multimetro[:5] + \
        (b'\xb044\xb6\xb511\xb01\r', b'', b'', b'')
    mock_ser.return_value.read.side_effect = registros
    mock_ser.return_value.is_open.return_value = False
    mock_res = mocker.Mock(escritos=10, erroneos=(6,))
    mock_guardar = mocker.patch(
        'unredat.Multimetro._guardar', autospec=True)
    mock_guardar.return_value = mock_res

    res = multi.recolectar_datos()
    mock_echo.assert_called_with(
        '\x1b[32mEscritura correcta de 10 valores en Uni-T_Datos.txt\x1b[0m')
    assert res == True


def test_recolectar_datos_puerto_erroneo(multi, mocker):
    """ Comprueba que al indicarle un puerto erróneo, se muetre el mensaje
    oportuno y se devuelve False.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """

    mock_echo = mocker.patch('click.echo', autospec=True)
    res = multi.recolectar_datos(puerto='/dev/inexistente')
    mock_echo.assert_called_with(
        '\x1b[31mError: No se puede conectar con /dev/inexistente\x1b[0m')
    assert res == False


def test_recolectar_datos_sin_multimetro(multi, mocker):
    """ Comprueba que al no encontrar el dispositovo USB de forma automática,
    se muetre el mensaje oportuno y se devuelve False.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """
    mock_comports = mocker.patch('serial.tools.list_ports.comports')
    mock_device = mocker.Mock(device='/dev/ttyUSB0', vid=0x1a23, pid=0x7487)
    mock_comports.return_value = [mock_device]
    mock_echo = mocker.patch('click.echo', autospec=True)

    res = multi.recolectar_datos()
    mock_echo.assert_called_with('\x1b[31mMultímetro NO encontrado.\x1b[0m')
    assert res == False


def test_crear_tabla(multi, tmpdir, mocker):
    """ Comprueba que se crea la tabla con los datos en el archivo
    correspondiente.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param tmpdir: permite crear directorios y archivos temporales
    :type tmpdir: pytest fixture
    :param mocker: permite trabajar con mocks
    :type mocker: pytest fixture
    """

    archivo_correcto = (' -  0.0023     DCV        4 V       OFF     AUTO  \n',
                        ' -  0.0023     DCV        4 V       OFF     AUTO  \n',
                        ' -  0.0024     DCV        4 V       OFF     AUTO  \n',
                        ' -  0.0023     DCV        4 V       OFF     AUTO  \n',
                        ' -  0.0024     DCV        4 V       OFF     AUTO  \n',
                        ' -  0.0016     DCV        4 V       OFF     AUTO  \n',
                        ' +  0.4465     DCV        4 V       OFF     AUTO  \n',
                        ' +  1.9787     DCV        4 V       OFF     AUTO  \n',
                        ' +  2.5647     DCV        4 V       OFF     AUTO  \n',
                        ' +  3.5918     DCV        4 V       OFF     AUTO  \n',
                        ' +  4.8830     DCV        40 V      OFF     AUTO  \n',
                        ' +  6.2200     DCV        40 V      OFF     AUTO  \n',
                        ' +  6.7730     DCV        40 V      OFF     AUTO  \n',
                        ' +  8.2170     DCV        40 V      OFF     AUTO  \n',
                        ' +  9.6000     DCV        40 V      OFF     AUTO  \n',
                        ' + 11.1330     DCV        40 V      OFF     AUTO  \n',
                        ' + 12.7840     DCV        40 V      OFF     AUTO  \n',
                        ' + 13.9620     DCV        40 V      OFF     AUTO  \n',
                        ' + 15.1340     DCV        40 V      OFF     AUTO  \n',
                        ' + 16.9880     DCV        40 V      OFF     AUTO  \n')

    fichero = tmpdir.mkdir('unredat').join('datos.txt')
    with open(str(fichero), 'w') as f:
        for dato in valores_correctos[:20]:
            f.write(''.join([dato, '\n']))

    mock_formatear = mocker.patch(
        'unredat.Multimetro._formatear_registro', autospec=True)
    mock_formatear.side_effect = archivo_correcto

    res = multi.crear_tabla(str(fichero))

    directorio = os.path.dirname(str(fichero))
    with open(''.join([directorio, os.sep, 'datos_tabla.txt']), 'r') as f:
        f.readline()  # Descarto la cabezera
        f.readline()  # Descarto la linea de puntos
        lineas_fichero = f.readlines()
        for n, linea in enumerate(lineas_fichero):
            assert linea == archivo_correcto[n]

    assert res == True


def test_crear_tabla_fichero_inexistente(multi, tmpdir):
    """ Comprueba que falla la creación de la tabla al indicar un archivo
    inexistente.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    :param tmpdir: permite crear directorios y archivos temporales
    :type tmpdir: pytest fixture
    """

    fichero = tmpdir.mkdir('unredat').join('inexistente.txt')
    res = multi.crear_tabla(str(fichero))
    assert res == False


def test_formatear_registros(multi):
    """ Pruba que varios casos de registros devuelve el formato esperado.

    :param multi: instancia de Multimetro
    :type multi: pytest fixture
    """

    res = multi._formatear_registro('16988', '21', '0', '1')
    assert res == ' + 16.9880     DCV        40 V      OFF     AUTO  \n'
    res = multi._formatear_registro('00588', '22', '3', '4')
    assert res == ' -  0.5880     ACV        40 V     AC+DC    OFF   \n'
    res = multi._formatear_registro('12500', '18', '0', '1')
    assert res == ' + 125.0000     mA       400 mA     OFF     AUTO  \n'
    res = multi._formatear_registro('00050', '212', '0', '2')
    assert res == ' +  5.0000      Hz       4 Khz      OFF    Manual \n'
    res = multi._formatear_registro('0124', '11', '0', '1')
    assert res == ' +  0.0124     DCV        4 V       OFF     AUTO  \n'


def test_main_sin_opciones(runner, mock_multi):
    """ Comprueba las llamadas de main sin parámetros.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param mock_multi: instancia del mock de clase Multimetro.
    :type mock_multi: pytest fixture
    """

    res = runner.invoke(unredat.main, [''])
    assert res.exit_code == 0
    assert mock_multi.recolectar_datos.called
    assert res.output == ''


def test_main_tabla_archivo_inexistente(runner, mock_multi):
    """ Comprueba main al intentar crear una tabla con un archivo
    inexistente.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param mock_multi: instancia del mock de clase Multimetro.
    :type mock_multi: pytest fixture
    """

    res = runner.invoke(unredat.main, ['-t', 'archivo.txt'])
    assert res.exit_code == 0
    assert mock_multi.crear_tabla.assert_not_called
    assert 'El fichero archivo.txt no existe o no es accesible' in res.output


def test_main_tabla_archivo(runner, mock_multi, fichero):
    """ Comprueba main al crear una tabla con un archivo válido.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param mock_multi: instancia del mock de clase Multimetro.
    :type mock_multi: pytest fixture
    :param fichero: fichero temporal par pruebas.
    :type fichero: pytest fixture
    """

    res = runner.invoke(unredat.main, ['-t', fichero])
    assert res.exit_code == 0
    assert mock_multi.crear_tabla.called
    assert 'Tabla creada correctamente' in res.output


def test_main_captura(runner, mock_multi):
    """ Comprueba main con la opción -c, --captura.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param mock_multi: instancia del mock de clase Multimetro.
    :type mock_multi: pytest fixture
    """

    res = runner.invoke(unredat.main, ['-c'])
    assert res.exit_code == 0
    assert mock_multi.recolectar_datos.called
    assert res.output == ''


def test_main_captura_y_tabla(runner, mock_multi, fichero):
    """ Comprueba main  con las opciones --captura y --tabla a la vez.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param mock_multi: instancia del mock de clase Multimetro.
    :type mock_multi: pytest fixture
    :param fichero: fichero temporal par pruebas.
    :type fichero: pytest fixture
    """

    res = runner.invoke(unredat.main, ['-c', '-t', fichero])
    assert res.exit_code == 0
    assert mock_multi.recolectar_datos.called
    assert mock_multi.crear_tabla.called
    assert 'Tabla creada correctamente' in res.output


def test_main_tinicio(runner, fichero):
    """ Comprueba main con la opcion --tinicio y --intervalo. Tanto con una
    fecha y hora válida como con una errónea.

    :param runner: instancia de click.Runner para hacer tests.
    :type runner: pytest fixture
    :param fichero: fichero temporal par pruebas.
    :type fichero: pytest fixture
    """

    res = runner.invoke(
        unredat.main, ['--tabla', '--tinicio', '19/07/18 13:36:11', fichero])

    fichero_tabla = ''.join(
        [os.path.dirname(fichero), os.sep, 'datos_tabla.txt'])
    with open(fichero_tabla, 'r') as f:
        f.readline()  # Cabezera
        f.readline()  # Línea de puntos
        linea = f.readline()

    assert res.exit_code == 0
    assert linea == ' +  2.5647     DCV        4 V       OFF     AUTO      13:36:12 19/07/18   \n'

    res = runner.invoke(unredat.main, ['--tabla', '--tinicio',
                                       '19/07/18 13:36:11', '--intervalo', 75, fichero])
    with open(fichero_tabla, 'r') as f:
        f.readline()  # Cabezera
        f.readline()  # Línea de puntos
        linea = f.readline()

    assert res.exit_code == 0
    assert linea == ' +  2.5647     DCV        4 V       OFF     AUTO      13:37:26 19/07/18   \n'

    res = runner.invoke(
        unredat.main, ['--tabla', '--tinicio', '34/07/18 24:36:11', fichero])

    assert res.exit_code == 2
    assert 'Invalid value' in res.output
